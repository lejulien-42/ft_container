# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lejulien <lejulien@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/08/13 01:11:27 by lejulien          #+#    #+#              #
#    Updated: 2022/03/15 15:46:47 by lejulien         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRCS = ./tester/main.cpp

SRCS_OBJS = ${SRCS:.cpp=.o}

FLAGS = -Wall -Wextra -Werror -std=c++98

DEBUG_FLAG = -Wall -Wextra -Werror -std=c++98 -g3 -fsanitize=address

.cpp.o:
	g++ $(FLAGS) -c $< -o $(<:.cpp=.o)

NAME = Test

$(NAME):
	@echo "\033[94m"
	@echo "\033[31m   ▄████████     ███               \033[94m ▄████████  ▄██████▄  ███▄▄▄▄       ███        ▄████████  ▄█  ███▄▄▄▄      ▄████████    ▄████████ "
	@echo "\033[31m  ███    ███ ▀█████████▄           \033[94m███    ███ ███    ███ ███▀▀▀██▄ ▀█████████▄   ███    ███ ███  ███▀▀▀██▄   ███    ███   ███    ███ "
	@echo "\033[31m  ███    █▀     ▀███▀▀██           \033[94m███    █▀  ███    ███ ███   ███    ▀███▀▀██   ███    ███ ███▌ ███   ███   ███    █▀    ███    ███ "
	@echo "\033[31m ▄███▄▄▄         ███   ▀           \033[94m███        ███    ███ ███   ███     ███   ▀   ███    ███ ███▌ ███   ███  ▄███▄▄▄      ▄███▄▄▄▄██▀ "
	@echo "\033[31m▀▀███▀▀▀         ███               \033[94m███        ███    ███ ███   ███     ███     ▀███████████ ███▌ ███   ███ ▀▀███▀▀▀     ▀▀███▀▀▀▀▀   "
	@echo "\033[31m  ███            ███               \033[94m███    █▄  ███    ███ ███   ███     ███       ███    ███ ███  ███   ███   ███    █▄  ▀███████████ "
	@echo "\033[31m  ███            ███               \033[94m███    ███ ███    ███ ███   ███     ███       ███    ███ ███  ███   ███   ███    ███   ███    ███ "
	@echo "\033[31m  ███           ▄████▀   ▀██████▀  \033[94m████████▀   ▀██████▀   ▀█   █▀     ▄████▀     ███    █▀  █▀    ▀█   █▀    ██████████   ███    ███ "
	@echo "\033[31m                                                                                                 BY : LEJULIEN  \033[94m          ███    ███ "
	@echo "\033[37m"
	g++ $(FLAGS) -o ft -D NAMESPACE=ft $(SRCS)
	g++ $(FLAGS) -o std -D NAMESPACE=std $(SRCS)

clean:
	rm -f $(SRCS_OBJS)

fclean: clean
	rm -fr ft .ft
	rm -fr std .std
	rm -fr ./srcs/mli

debug: fclean
	g++ $(DEBUG_FLAG) -DNAME -o $(NAME) $(SRCS)
	./$(NAME)

diff:
	./ft > .ft
	./std > .std
	diff .ft .std

mli:
	git clone https://github.com/mli42/containers_test.git ./srcs/mli

rm-mli:
	rm -fr ./srcs/mli


re: fclean $(NAME)

.PHONY: clean fclean re
