//
//	Made by lejulien
//

#ifndef ITERATOR_TRAITS_HPP
# define ITERATOR_TRAITS_HPP
#include <stddef.h>
namespace ft
{
	struct input_iterator_tag { };
	struct output_iterator_tag { };
	struct forward_iterator_tag : public input_iterator_tag { };
	struct bidirectional_iterator_tag : public forward_iterator_tag { };
	struct random_access_iterator_tag : public bidirectional_iterator_tag { };
	struct contiguous_iterator_tag: public random_access_iterator_tag { };

	template<typename _category, typename _tp, typename _distance = ptrdiff_t, typename _pointer = _tp*, typename _reference = _tp&>
	struct iterator
	{
		typedef _category	iterator_category;
		typedef _tp			value_type;
		typedef _distance	difference_type;
		typedef _pointer	pointer;
		typedef _reference	reference;
	};

	template<class Iterator>
	struct iterator_traits
	{
		typedef random_access_iterator_tag iterator_category;
		typedef int value_type;
		typedef ptrdiff_t difference_type;
		typedef Iterator* pointer;
		typedef Iterator& reference;
	};

	template<typename T>
	struct iterator_traits<T*>
	{
		typedef random_access_iterator_tag iterator_category;
		typedef T value_type;
		typedef ptrdiff_t difference_type;
		typedef T* pointer;
		typedef T& reference;
	};

	template<class T>
	struct iterator_traits<const T*>
	{
		typedef random_access_iterator_tag iterator_category;
		typedef T value_type;
		typedef ptrdiff_t difference_type;
		typedef const T* pointer;
		typedef const T& reference;
	};
}
#endif

//	Made by lejulien@42
