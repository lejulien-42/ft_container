/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_pair.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lejulien <lejulien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/14 02:10:06 by lejulien          #+#    #+#             */
/*   Updated: 2021/08/26 06:03:41 by lejulien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAKE_PAIR_HPP
# define MAKE_PAIR_HPP
# include "pair.hpp"
namespace ft {
	template< class T1, class T2 >
	ft::pair<T1,T2> make_pair( T1 t, T2 u ) {
		ft::pair<T1,T2> p(t, u);
		return p;
	};
};

#endif
