/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conditional.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lejulien <lejulien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/19 16:43:13 by lejulien          #+#    #+#             */
/*   Updated: 2021/11/27 20:50:51 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONDITIONAL_HPP
# define CONDITIONAL_HPP

namespace ft {
	template <bool b, class T = void, class V = void>
	struct conditional {};

	template <class T, class V>
	struct conditional<true, T, V> {
		typedef T type;
	};

	template <class T, class V>
	struct conditional<false, T, V> {
		typedef V type;
	};

	template <class T>
	void swap (T & x, T & y)
	{
		T	tmp(x);
		x = y;
		y = tmp;
	}
};
#endif
