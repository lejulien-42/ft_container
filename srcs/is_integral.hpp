/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_integral.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lejulien <lejulien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/14 02:26:50 by lejulien          #+#    #+#             */
/*   Updated: 2021/11/18 18:11:01 by lejulien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IS_INTEGRAL_HPP
# define IS_INTEGRAL_HPP
namespace ft {

	template <typename T, T val>
	struct container_type
	{
		static const T value = val;
	};

	struct false_type : public container_type<bool, false> {};
	struct true_type : public container_type<bool, true> {};

	template<class T>
	struct is_integral : public false_type {};

	template<>
	struct is_integral<int> : public true_type {};

	template<>
	struct is_integral<unsigned int> : public true_type {};

	template<>
	struct is_integral<bool> : public true_type {};

	template<>
	struct is_integral<char> : public true_type {};

	template<>
	struct is_integral<unsigned char> : public true_type {};

	template<>
	struct is_integral<signed char> : public true_type {};

	template<>
	struct is_integral<short> : public true_type {};

	template<>
	struct is_integral<unsigned short> : public true_type {};

	template<>
	struct is_integral<long> : public true_type {};

	template<>
	struct is_integral<unsigned long> : public true_type {};

	template<>
	struct is_integral<long long> : public true_type {};

	template<>
	struct is_integral<unsigned long long> : public true_type {};
};
#endif
