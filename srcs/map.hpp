#ifndef MAP_HPP
# define MAP_HPP
#include <functional>
#include <memory>
#include "pair.hpp"
#include "make_pair.hpp"
#include "conditional.hpp"
#include "is_integral.hpp"
#include "reverse_iterator.hpp"
#include "enable_if.hpp"
#include "lexicographical_compare.hpp"

namespace ft
{
    template<class K, class V, class Compare = std::less<K>, class Alloc = std::allocator< ft::pair< const K, V > > >
    class map {
    public:

        typedef ft::pair<const K, V>  value_type;
        typedef K key_type;
        typedef V mapped_type;

        // SUBCLASSES

        typedef struct sLeaf {
        public:
            ft::pair<const K, V> values;
            struct sLeaf    *left;
            struct sLeaf    *right;
            struct sLeaf    *parent;
            bool    color;
            sLeaf    (ft::pair<const K, V> val) : values(val) {
            }
            const K &key() {
                return this->values.first;
            }
            const V &value() {
                return this->values.second;
            }
        }               Leaf;

        template<bool is_const>
        class mapIterator {
        public:
            typedef ft::pair<const K, V> pair_type;
            typedef typename ft::conditional<is_const, const pair_type, pair_type>::type value_type;
            typedef typename ft::conditional<is_const, const Leaf, Leaf>::type leaf_type;
            typedef std::ptrdiff_t difference_type;
            typedef std::size_t size_type;
            typedef pair_type* pointer;
            typedef pair_type* reference;
            typedef std::random_access_iterator_tag iterator_category;
        private:
            leaf_type   *ptr;
        public:

            // Constructors

            mapIterator() {
                ptr = NULL;
            }
            mapIterator(leaf_type *optr) {
                this->ptr = optr;
            }
            template<bool b_const>
            mapIterator(const mapIterator<b_const> & other, typename ft::enable_if<!b_const>::type * = 0) {
                ptr = other.get_ptr();
            }
            ~mapIterator() {

            }

            // Pointer stuff

            leaf_type *get_ptr() const {
                return ptr;
            }
            value_type *operator->() const {
                return &ptr->values;
            }
            value_type &operator*() const {
                return ptr->values;
            }

            // Overloads
            public:
            template<bool b_const>
            bool    operator==(const mapIterator<b_const> &other) const {
                return ptr == other.get_ptr();
            }
            template<bool b_const>
            bool    operator!=(const mapIterator<b_const> &other) const {
                return ptr != other.get_ptr();
            }
            mapIterator &operator=(const mapIterator &other) {
                ptr = other.get_ptr();
                return *this;
            }
            mapIterator &operator++() {
                this->next_leaf();
                return *this;
            }
            mapIterator &operator--() {
                this->prev_leaf();
                return *this;
            }
            mapIterator operator++(int) {
                mapIterator<is_const> ret(*this);
                operator++();
                return ret;
            }
            mapIterator operator--(int) {
                mapIterator<is_const> ret(*this);
                operator--();
                return ret;
            }

        private:

            // Intern functions

            void next_leaf(void)
            {
                if (ptr->right != ptr->right->parent)
                {
                    ptr = ptr->right;
                    while (ptr->left != ptr->left->parent) {
                        ptr = ptr->left;
                    }
                }
                else
                {
		    		while (ptr == ptr->parent->right && ptr != ptr->parent) {
                        ptr = ptr->parent;
                    }
		    		ptr = ptr->parent;
                }
            }

            void prev_leaf(void)
            {
                if (ptr == ptr->parent)
                {
                    ptr = ptr->right;
                    while (ptr->right != ptr->right->parent)
                        ptr = ptr->right;

                }
                else if (ptr->left != ptr->left->parent)
                {
                    ptr = ptr->left;
                    while (ptr->right != ptr->right->parent)
                        ptr = ptr->right;
                }
                else
                {
                    while (ptr == ptr->parent->left && ptr != ptr->parent)
                        ptr = ptr->parent;
                    ptr = ptr->parent;
                }
            }

        };

        class ValueCompare {
            protected:
                ValueCompare(Compare c) : comp(c) {};
                Compare comp;
            public:
                friend class        map;
                typedef bool        result_type;
                typedef value_type  first_argument_type;
                typedef value_type  second_argument_type;
                bool    operator()(const value_type &first, const value_type &second) const {
                    return comp(first.first, second.first);
                }
        };

        public:
        typedef Compare key_cmp;
        typedef Compare key_compare;
        typedef ValueCompare val_cmp;
        typedef	typename Alloc::template rebind<Leaf>::other	allocator_type;
        typedef typename allocator_type::reference               reference;
        typedef typename allocator_type::const_reference         const_reference;
        typedef typename allocator_type::pointer                 pointer;
        typedef typename allocator_type::const_pointer           const_pointer;
        typedef mapIterator<false>                      iterator;
        typedef mapIterator<true>                       const_iterator;
        typedef ft::reverse_iterator<iterator>          reverse_iterator;
        typedef ft::reverse_iterator<const_iterator>    const_reverse_iterator;
        typedef typename mapIterator<false>::difference_type    difference_type;
        typedef typename mapIterator<false>::size_type          size_type;
        allocator_type alloc;
        key_cmp comp;
        Leaf    *base;
        
        // Constructors / Destructors


        template<class InputIt>
        map(InputIt first, InputIt last, const key_cmp &comp = key_cmp(), const allocator_type &alloc = allocator_type(), typename ft::enable_if<!ft::is_equal<InputIt, int>::value >::type* = 0) {
            this->alloc = alloc;
            this->comp = comp;
            this->new_empty_node();
            while (first != last)
                this->insert(*first++);
        }

        explicit map(const key_cmp &comp = key_cmp(), const allocator_type &alloc = allocator_type()) {
            this->alloc = alloc;
            this->comp = comp;
            this->new_empty_node();
        }

        map(const map &src) {
            this->new_empty_node();
            *this = src;
        }

        ~map() {
            this->clear();
            this->alloc.destroy(this->base);
            this->alloc.deallocate(this->base, 1);
        }

        // Member functions

        size_type count(const key_type &key) const {
            size_type size = 0;
            for (const_iterator it = this->begin(); it != this->end(); it++) {
                if (this->is_equal(key, it->first))
                    size++;
            }
            return size;
        }

        iterator find(const key_type &key) {
            if (this->count(key))
                return iterator(this->findLeaf(this->base->right, key));
            return this->end();
        }

        const_iterator find(const key_type &key) const {
            if (this->count(key))
                return const_iterator(this->findLeaf(this->base->right, key));
            return this->end();
        }

        ft::pair<iterator, bool> insert(const value_type &value) {
            iterator it;

            if (this->count(value.first)) {
                it = this->find(value.first);
                return ft::make_pair(it, false);
            }
            it = iterator(this->newLeaf(value));
            return ft::make_pair(it, true);
        }

        iterator insert(iterator it, const value_type &value) {
            (void)it;
            return this->insert(value).first;
        }

        template<class InputIT>
        void insert(InputIT first, InputIT second, typename ft::enable_if<!ft::is_equal<InputIT, int>::value >::type* = 0) {
            while (first != second)
                this->insert(*first++);
        }

        void erase(iterator it) {
            Leaf *ptr = it.get_ptr();
            Leaf *child;

            if (ptr->left != this->base)
                child = ptr->left;
            else
                child = ptr->right;
            if (ptr->left != this->base && ptr->right != this->base) {
                it--;
                this->swapLeafs(ptr, it.get_ptr());
                this->erase(ptr);
            }
            else {
                if (child != this->base)
                    child->parent = ptr->parent;
                if (ptr->parent->left == ptr)
                    ptr->parent->left = child;
                else
                    ptr->parent->right = child;
                this->removeLeaf(ptr, child);
            }
        }

        size_type erase(const key_type &key) {
            if (this->count(key)) {
                this->erase(this->find(key));
                return (1);
            }
            return (0);
        }

        void erase(iterator first, iterator last) {
            for (iterator it = first++; it != last; it = first++)
                this->erase(it);
        }

        void clear() {
            iterator first = this->begin();
            for (iterator it = first++; it != this->end(); it = first++)
                this->erase(it);
        }

        void swap(map &other) {
            ft::swap(this->alloc, other.alloc);
            ft::swap(this->comp, other.comp);
            ft::swap(this->base, other.base);
        }

        key_cmp key_comp() const {
            return key_cmp();
        }

        val_cmp value_comp() const {
            return val_cmp(this->comp);
        }

        // Bounds

        iterator lower_bound(const key_type &key) {
            iterator it = this->begin();
            while (this->comp(it->first, key) && it != this->end())
                it++;
            return it;
        }

        const_iterator lower_bound(const key_type &key) const {
            const_iterator it = this->begin();
            while (this->comp(it->first, key) && it != this->end())
                it++;
            return it;
        }

        iterator upper_bound(const key_type &key) {
            iterator it = this->begin();
            while (this->comp(key, it->first) == false && it != this->end())
                it++;
            return it;
        }

        const_iterator upper_bound(const key_type &key) const {
            const_iterator it = this->begin();
            while (this->comp(key, it->first) == false && it != this->end())
                it++;
            return it;
        }

        ft::pair<iterator, iterator> equal_range(const key_type &key) {
            return ft::make_pair(this->lower_bound(key), this->upper_bound(key));
        }

        ft::pair<const_iterator, const_iterator> equal_range(const key_type &key) const {
            return ft::make_pair(this->lower_bound(key), this->upper_bound(key));
        }

        // Alloc functions

        bool empty() const {
            return (this->base == this->base->right);
        }

        size_type size() const {
            size_type size = 0;
            for (const_iterator it = this->begin(); it != this->end(); it++)
                size++;
            return size;
        }

        size_type max_size() const {
            return alloc.max_size();
        }

        // Iterators

        iterator begin() {
            return (iterator(this->smallest(this->base->right)));
        }

        const_iterator begin() const {
            return (const_iterator(this->smallest(this->base->right)));
        }

        iterator end() {
            return (iterator(this->base));
        }

        const_iterator end() const {
            return (const_iterator(this->base));
        }

        reverse_iterator rbegin() {
            return reverse_iterator(this->base);
        }

        const_reverse_iterator rbegin() const {
            return const_reverse_iterator(this->base);
        }

        reverse_iterator rend() {
            return (reverse_iterator(this->smallest(this->base->right)));
        }

        const_reverse_iterator rend() const {
            return (const_reverse_iterator(this->smallest(this->base->right)));
        }

        // Accessors

        mapped_type &operator[](const key_type &key) {
            this->insert(ft::make_pair(key, mapped_type()));
            return (this->find(key)->second);
        }

        allocator_type get_allocator() const {
            return allocator_type();
        }

    private:

        void leftLeftRotate(Leaf *grandparent, Leaf *parent) {
            if (grandparent->parent->right == grandparent)
                grandparent->parent->right = parent;
            else
                grandparent->parent->left = parent;
            if (parent->right != this->base)
                parent->right->parent = grandparent;
            grandparent->left = parent->right;
            parent->parent = grandparent->parent;
            grandparent->parent = parent;
            parent->right = grandparent;
        }

        void rightRightRotate(Leaf *grandparent, Leaf *parent) {
            if (grandparent->parent->right == grandparent)
                grandparent->parent->right = parent;
            else
                grandparent->parent->left = parent;
            if (parent->left != this->base)
                parent->left->parent = grandparent;
            grandparent->right = parent->left;
            parent->parent = grandparent->parent;
            grandparent->parent = parent;
            parent->left = grandparent;
        }

        void leftRightRotate(Leaf *grandparent, Leaf *parent, Leaf *leaf) {
            if (grandparent->parent->right == grandparent)
                grandparent->parent->right = leaf;
            else
                grandparent->parent->left = leaf;
            if (leaf->left != this->base)
                leaf->left->parent = parent;
            if (leaf->right != this->base)
                leaf->right->parent = grandparent;
            grandparent->left = leaf->right;
            parent->right = leaf->left;
            leaf->parent = grandparent->parent;
            grandparent->parent = leaf;
            parent->parent = leaf;
            leaf->left = parent;
            leaf->right = grandparent;
        }

        void rightLeftRotate(Leaf *grandparent, Leaf *parent, Leaf *leaf) {
            if (grandparent->parent->right == grandparent)
                grandparent->parent->right = leaf;
            else
                grandparent->parent->left = leaf;
            if (leaf->left != this->base)
                leaf->left->parent = grandparent;
            if (leaf->right != this->base)
                leaf->right->parent = parent;
            grandparent->right = leaf->left;
            parent->left = leaf->right;
            leaf->parent = grandparent->parent;
            grandparent->parent = leaf;
            parent->parent = leaf;
            leaf->left = grandparent;
            leaf->right = parent;
        }

        void insertion(Leaf *leaf) {
            Leaf *parent = leaf->parent;
            Leaf *grandparent = parent->parent;
            Leaf *aunt;

            if (grandparent->right == parent)
                aunt = grandparent->left;
            else
                aunt = grandparent->right;
            if (parent == this->base)
                leaf->color = false;
            else if (parent->color == false)
                return ;
            else if (aunt->color == true) {
                parent->color = false;
                aunt->color = false;
                grandparent->color = true;
                this->insertion(grandparent);
            }
            else {
                if (grandparent->left->left == leaf || grandparent->right->right == leaf) {
                    if (grandparent->left->left == leaf)
                        this->leftLeftRotate(grandparent, parent);
                    else if (grandparent->right->right == leaf)
                        this->rightRightRotate(grandparent, parent);
                    ft::swap(grandparent->color, parent->color);
                }
                else {
                    if (grandparent->left->right == leaf)
                        this->leftRightRotate(grandparent, parent, leaf);
                    else if (grandparent->right->left == leaf)
                        this->rightLeftRotate(grandparent, parent, leaf);
                    ft::swap(grandparent->color, leaf->color);
                }
            }
        }

        void create(Leaf *ptr, const value_type &src = value_type()) {
            Leaf tmp(src);
            tmp.left = this->base;
            tmp.right = this->base;
            tmp.parent = this->base;
            tmp.color = true;
            this->alloc.construct(ptr, tmp);
        }

        Leaf *findLeaf(Leaf *leaf, const key_type &key) const {
            if (leaf == this->base || this->is_equal(leaf->key(), key))
                return leaf;
            else if (this->comp(key, leaf->key()))
                return this->findLeaf(leaf->left, key);
            return this->findLeaf(leaf->right, key);
        }

        Leaf *findParent(Leaf *leaf, const key_type &key) const {
            if (!this->comp(key, leaf->key())) {
                if (leaf->right == this->base)
                    return leaf;
                return this->findParent(leaf->right, key);
            }
            if (leaf->left == this->base)
                return leaf;
            return this->findParent(leaf->left, key);
        }

        Leaf *newLeaf(const value_type &value = value_type()) {
            Leaf *leaf = alloc.allocate(1);
            Leaf *parent = this->findParent(this->base->right, value.first);

            this->create(leaf, value);
            if (parent == this->base || !this->comp(value.first, parent->key()))
                parent->right = leaf;
            else
                parent->left = leaf;
            leaf->parent = parent;
            this->insertion(leaf);
            return (leaf);
        }

        void new_empty_node() {
            this->base = this->alloc.allocate(1);
            this->create(this->base);
            this->base->color = false;
        }

        Leaf    *smallest(Leaf *base) const {
            while (base->left != base->left->left)
                base = base->left;
            return base;
        }

        bool is_equal(const key_type &first, const key_type &second) const {
            return (this->comp(first, second) == false && this->comp(second, first) == false);
        }

        void swapLeafs(Leaf *x, Leaf *y) {
		    if (x->left != y && x->left != this->base)
			    x->left->parent = y;
		    if (x->right != y && x->right != this->base)
			    x->right->parent = y;
		    if (x->parent != y && x->parent != this->base) {
			    if (x->parent->left == x)
		    		x->parent->left = y;
			    else
			    	x->parent->right = y;
            }
		    if (y->left != x && y->left != this->base)
		    	y->left->parent = x;
		    if (y->right != x && y->right != this->base)
			    y->right->parent = x;
		    if (y->parent != x && y->parent != this->base) {
			    if (y->parent->left == y)
				    y->parent->left = x;
			    else
			    	y->parent->right = x;
		    }
		    if (x->parent == y)
			    x->parent = x;
		    if (x->left == y)
			    x->left = x;
		    if (x->right == y)
			    x->right = x;
		    if (y->parent == x)
		    	y->parent = y;
		    if (y->left == x)
	    		y->left = y;
		    if (y->right == x)
		    	y->right = y;
		    ft::swap(x->parent, y->parent);
		    ft::swap(x->left, y->left);
		    ft::swap(x->right, y->right);
		    ft::swap(x->color, y->color);
		    if (this->base->right == x)
		    	this->base->right = y;
		    else if (this->base->right == y)
		    	this->base->right = x;
        }

        void deleteRecurse(Leaf *aunt, Leaf *parent) {
            Leaf *tmp;

            if (parent->left != aunt)
                tmp = parent->left;
            else
                tmp = parent->right;
            if (aunt == this->base->right)
                return ;
            else if (tmp->color == false && (tmp->left->color == true || tmp->right->color == true)) {
                if (tmp == parent->left && tmp->left->color == true)
                    this->leftLeftRotate(parent, tmp);
                else if (tmp == parent->left && tmp->right->color == true)
                    this->leftRightRotate(parent, tmp, tmp->right);
                else if (tmp == parent->right && tmp->right->color == true)
                    this->rightRightRotate(parent, tmp);
                else if (tmp == parent->right && tmp->left->color == true)
                    this->rightLeftRotate(parent, tmp, tmp->left);
                if (tmp->left->color == true)
                    tmp->left->color = false;
                else
                    tmp->right->color = false;
            }
            else if (tmp->color == true) {
                if (tmp == parent->left)
                    this->leftLeftRotate(parent, tmp);
                else
                    this->rightRightRotate(parent, tmp);
                ft::swap(parent->color, tmp->color);
                this->deleteRecurse(aunt, parent);
            }
            else if (tmp->color == false) {
                tmp->color = true;
                if (parent->color == true)
                    parent->color = false;
                else
                    this->deleteRecurse(parent, parent->parent);
            }
        }

        void removeLeaf(Leaf *ptr, Leaf *child) {
            if (ptr->color == true || child->color == true)
                child->color = false;
            else
                this->deleteRecurse(child, ptr->parent);
            this->alloc.destroy(ptr);
            this->alloc.deallocate(ptr, 1);
        }

        // Operators
        public:
        map &operator=(const map &other) {
            if (this == &other)
                return *this;
            this->clear();
            this->alloc = other.alloc;
            this->comp = other.comp;
            for (const_iterator it = other.begin(); it != other.end(); it++)
                this->insert(*it);
            return *this;
        }
    };

    // non-member functions

	template <class Key, class Value, class Compare, class Alloc>
	void swap(map<Key,Value,Compare,Alloc> & x, map<Key,Value,Compare,Alloc> & y) {
		x.swap(y);
	}

    // Operators

	template <class Key, class Value, class Compare, class Alloc>
	bool operator==(const map<Key,Value,Compare,Alloc> & lhs, const map<Key,Value,Compare,Alloc> & rhs) {
		return ft::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
	}

	template <class Key, class Value, class Compare, class Alloc>
	bool operator!=(const map<Key,Value,Compare,Alloc> & lhs, const map<Key,Value,Compare,Alloc> & rhs) {
		return !(lhs == rhs);
	}

	template <class Key, class Value, class Compare, class Alloc>
	bool operator<(const map<Key,Value,Compare,Alloc> & lhs, const map<Key,Value,Compare,Alloc> & rhs) {
		return ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
	}

	template <class Key, class Value, class Compare, class Alloc>
	bool operator>(const map<Key,Value,Compare,Alloc> & lhs, const map<Key,Value,Compare,Alloc> & rhs) {
		return rhs < lhs;
	}

	template <class Key, class Value, class Compare, class Alloc>
	bool operator<=(const map<Key,Value,Compare,Alloc> & lhs, const map<Key,Value,Compare,Alloc> & rhs) {
		return !(rhs < lhs);
	}

	template <class Key, class Value, class Compare, class Alloc>
	bool operator>=(const map<Key,Value,Compare,Alloc> & lhs, const map<Key,Value,Compare,Alloc> & rhs) {
		return !(lhs < rhs);
	}
};

#endif
