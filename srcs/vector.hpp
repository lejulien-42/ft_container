/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lejulien <lejulien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/16 03:12:10 by lejulien          #+#    #+#             */
/*   Updated: 2021/12/01 05:16:46 by lejulien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTOR_HPP
# define VECTOR_HPP
#include <memory>
#include <stdexcept>
#include "conditional.hpp"
#include "is_integral.hpp"
#include "reverse_iterator.hpp"
#include "enable_if.hpp"
#include "lexicographical_compare.hpp"

namespace ft {
	template< class T, class Alloc = std::allocator<T> >
	class vector {
		public:
			// Iterators

			template <bool is_const>
			class vectorIterator {
				public:
					typedef typename ft::conditional<is_const, const T, T>::type	value_type;
				protected:
					value_type	*_ptr;
				public:
					typedef std::ptrdiff_t											difference_type;
					typedef std::size_t												size_type;
					typedef value_type* pointer;
					typedef value_type& reference;
					typedef std::random_access_iterator_tag iterator_category;
					// Constructors

					vectorIterator() {
						_ptr = NULL;
					}
					~vectorIterator() {}
					template<bool b>
					vectorIterator(const vectorIterator<b> &rhs, typename ft::enable_if<!b>::type* = 0 ) {
						_ptr = rhs.get_ptr();
					}
					vectorIterator(value_type * const optr) {
						_ptr = optr;
					}
					// Getter

					value_type	*get_ptr() const {
						return _ptr;
					}

					// Operators

					vectorIterator&	operator=(const vectorIterator &rhs) {
						_ptr = rhs.get_ptr();
						return *this;
					}
					vectorIterator&	operator+=(int n) {
						_ptr += n;
						return *this;
					}
					vectorIterator&	operator-=(int n) {
						_ptr -= n;
						return *this;
					}

					vectorIterator	&operator++() {
						_ptr++;
						return *this;
					}
					vectorIterator	&operator--() {
						_ptr--;
						return *this;
					}
					vectorIterator	operator++(int) {
						vectorIterator<is_const> out(*this);
						_ptr++;
						return out;
					}
					vectorIterator	operator--(int) {
						vectorIterator<is_const> out(*this);
						_ptr--;
						return out;
					}
					vectorIterator	operator+(int n) const {
						return _ptr + n;
					}
					vectorIterator	operator-(int n) const {
						return _ptr - n;
					}
					difference_type	operator-(const vectorIterator &rhs) const {
						return _ptr - rhs.get_ptr();
					}
					friend vectorIterator	operator+(int n, const vectorIterator &rhs) {
						return rhs.get_ptr() + n;
					}
					
					template <bool b>
					bool	operator==(const vectorIterator<b> &rhs) const {
						return _ptr == rhs.get_ptr();	
					}
					bool	operator==(value_type * v) const {
						return _ptr == v;	
					}
					template <bool b>
					bool	operator!=(const vectorIterator<b> &rhs) const {
						return _ptr != rhs.get_ptr();
					}
					template <bool b>
					bool	operator<(const vectorIterator<b> &rhs) const {
						return _ptr < rhs.get_ptr();
					}
					template <bool b>
					bool	operator>(const vectorIterator<b> &rhs) const {
						return _ptr > rhs.get_ptr();
					}
					template <bool b>
					bool	operator<=(const vectorIterator<b> &rhs) const {
						return _ptr <= rhs.get_ptr();
					}
					template <bool b>
					bool	operator>=(const vectorIterator<b> &rhs) const {
						return _ptr >= rhs.get_ptr();
					}

					value_type	*operator->() const {
						return _ptr;
					}
					value_type	&operator[](size_type n) const {
						return *(_ptr + n);
					}
					value_type	&operator*() const {
						return *_ptr;
					}
			};

			typedef T												value_type;
			typedef Alloc											allocator_type;
			typedef std::ptrdiff_t									difference_type;
			typedef std::size_t										size_type;
			typedef typename Alloc::reference						reference;
			typedef typename Alloc::const_reference					const_reference;
			typedef typename Alloc::pointer							pointer;
			typedef typename Alloc::const_pointer					const_pointer;

			// need vectorIterators
			typedef vectorIterator<false>					iterator;
			typedef vectorIterator<true>					const_iterator;
			typedef ft::reverse_iterator< iterator >				reverse_iterator;
			typedef ft::reverse_iterator< const_iterator >		const_reverse_iterator;

		protected:
			Alloc	_alloc;
			pointer	_vector;
			size_type	_size;
			size_type	_capacity;

		private:
			size_type alloc_cap(size_type n) {
				if (n > _capacity * 2 || _capacity == 0)
					return n;
				else if (n > _capacity && _capacity > 0)
					return _size * 2;
				return _capacity;
			}

		public:
			// Constructors
			explicit vector(const allocator_type& alloc = allocator_type()): _alloc(alloc), _size(0), _capacity(0)
			{
				_vector = _alloc.allocate(_capacity);
			}
			explicit vector (size_type n, const value_type& val = value_type(), const allocator_type& alloc = allocator_type()): _alloc(alloc), _size(n), _capacity(n)
			{
				_vector = _alloc.allocate(_capacity);

				for (size_type i = 0; i < n; i++)
					_alloc.construct(_vector + i, val);
			}
		
			template<class InputIt>	
         	vector (InputIt first, InputIt last, const allocator_type& alloc = allocator_type(), typename ft::enable_if< !ft::is_integral<InputIt>::value >::type* = 0): _alloc(alloc)
			{
				size_type count = 0;
				for (InputIt it = first; it != last && count <= this->max_size(); it++)
					count++;
				_capacity = count;
				_size = count;
				_vector = _alloc.allocate(_capacity);
				for (size_type i = 0; i < _size; i++) {
					_alloc.construct(&_vector[i], *first++);
				}
			}
			
			vector(const vector &other) {
				_alloc = other._alloc;
				_size = other._size;
				_capacity = other._size;
				_vector = _alloc.allocate(_size);
				for (size_type i = 0; i < _size; i++) {
					_alloc.construct(_vector + i, other[i]);
				}
			}

			// Destructor

			~vector() {
				for (size_type i = 0; i < _size; i++)
					_alloc.destroy(&_vector[i]);
				_alloc.deallocate(_vector, _capacity);
			}

			// Operator equal

			vector& operator=( const vector& other ) {
				if (this == &other)
					return *this;
				for (size_type i = 0; i < _size; i++)
					_alloc.destroy(_vector + i);
				_alloc = other._alloc;
				_size = other._size;
				if (other._size > _capacity) {
					_alloc.deallocate(_vector, _capacity);
					_capacity = other._size;
					_vector = _alloc.allocate(other._size);
				}
				for (size_type i = 0; i < _size; i++) {
					_alloc.construct(_vector + i, other[i]);
				}
				return *this;
			}

			// Iterators
			iterator begin() { return iterator(_vector);}
			const_iterator begin() const { return (const_iterator(_vector)); }
			iterator end() { return (iterator(_vector + _size)); }
			const_iterator end() const { return (const_iterator(_vector + _size)); }
			reverse_iterator rbegin() { return (reverse_iterator(this->end())); }
			const_reverse_iterator rbegin() const { return (const_reverse_iterator(this->end())); }
			reverse_iterator rend() { return (reverse_iterator(this->begin())); }
			const_reverse_iterator rend() const { return (const_reverse_iterator(this->begin())); }

			// Assign
			
			template< class InputIt>
			void assign( InputIt first, InputIt last, typename ft::enable_if< !ft::is_integral<InputIt>::value >::type* = 0 ) {
				clear();
				size_type size_diff = 0;
				for (InputIt it = first; it != last; it++)
					size_diff++;
				if (size_diff > _capacity) {
					reserve(size_diff);
					_size = _capacity;
					size_type i = 0;
					while (first != last) {
						_alloc.construct(_vector + i, *first);
						i++;
						first++;
					}
					return ;
				}
				while (first != last)
					push_back(*first++);
			}

			void assign( size_type count, const T& value ) {
				clear();
				if (count > _capacity) {
					reserve(count);
					_size = _capacity;
					for (size_type i = 0; i < count; i++)
						_alloc.construct(_vector + i, value);
					return ;
				}
				for (size_type i = 0; i < count; i++)
					push_back(value);
			}

			// Get_allocator

			allocator_type get_allocator() const {
				return _alloc();
			}

			// At

			reference at( size_type pos ) {
				if (pos >= _size)
					throw std::out_of_range("vector");
				return (_vector[pos]);
			}

			const_reference at( size_type pos ) const {
				if (pos >= _size)
					throw std::out_of_range("vector");
				return (_vector[pos]);
			}

			// [] Operator

			reference operator[]( size_type pos ) {
				return _vector[pos];
			}

			const_reference operator[]( size_type pos ) const {
				return (_vector[pos]);
			}

			// Front

			reference front() {
				return _vector[0];
			}

			const_reference front() const {
				return _vector[0];
			}

			// Back

			reference back() {
				return _vector[_size - 1];
			}

			const_reference back() const {
				return _vector[_size - 1];
			}

			// Data

			T* data() {
				return _vector;
			}

			const T* data() const {
				return _vector;
			}

			// Empty

			bool empty() const {
				return (this->begin() == this->end());
			}

			// Size

			size_type size() const {
				return _size;
			}

			// Max_size

			size_type max_size() const {
				return _alloc.max_size();
			}

			// Reserve

			void reserve( size_type new_cap ) {
				if (new_cap > max_size())
					throw std::length_error("vector::reserve");
				if (new_cap <= _capacity)
					return ;
				pointer temp = _alloc.allocate(new_cap);
				_capacity = new_cap;
				for (size_type i = 0; i < _size; i++) 
					_alloc.construct(temp + i, _vector[i]);
				for (size_type i = 0; i < _size; i++)
					_alloc.destroy(_vector + i);
				_alloc.deallocate(_vector, _capacity);
				_vector = temp;
			}

			// Capacity

			size_type capacity() const {
				return _capacity;
			}

			// Clear

			void clear() {
				for (size_type i = 0; i < _size; i++)
					_alloc.destroy(_vector + i);
				_size = 0;
			}

			// Insert
			
		iterator insert(iterator position, const value_type& val)
		{
			pointer tmp;

			tmp = _alloc.allocate(alloc_cap(_size + 1));
			iterator it = this->begin();
			size_type i = 0;
			size_type ret = 0;
			while (it != this->end())
			{
				if (it == position)
				{
					ret = i;
					_alloc.construct(tmp + i, val);
					i++;
				}
				_alloc.construct(tmp + i, *it);
				i++;
				it++;
			}
			if (position == this->end())
			{
				ret = i;
				_alloc.construct(tmp + i, val);
				i++;
			}
			for (size_type i = 0; i < _size; i++)
				_alloc.destroy(_vector + i);
			_alloc.deallocate(_vector, _capacity);
			_vector = tmp;
			_capacity = alloc_cap(_size + 1);
			_size = i;
			return tmp + ret;
		}

		void insert(iterator position, size_type n, const value_type& val)
		{
			pointer tmp;

			tmp = _alloc.allocate(alloc_cap(_size + n));
			iterator it = this->begin();
			size_type i = 0;
			while (it != this->end())
			{
				if (it == position)
				{
					for (size_type fill = 0; fill < n; fill++)
					{
						_alloc.construct(tmp + i, val);
						i++;
					}
				}
				_alloc.construct(tmp + i, *it);
				i++;
				it++;
			}
			if (position == this->end())
			{
				for (size_type fill = 0; fill < n; fill++)
				{
					_alloc.construct(tmp + i, val);
					i++;
				}
			}
			for (size_type i = 0; i < _size; i++)
				_alloc.destroy(_vector + i);
			_alloc.deallocate(_vector, _capacity);
			_capacity = alloc_cap(_size + n);
			_size = i;
			_vector = tmp;
		}

		template <class InputIterator>
		void insert(iterator position, InputIterator first, InputIterator last, typename ft::enable_if<!ft::is_integral<InputIterator>::value>::type* = 0)
		{
			pointer tmp;
			size_type n = 0;

			for (InputIterator len = first; len != last; len++)
				n++;
			tmp = _alloc.allocate(alloc_cap(_size + n));
			iterator it = this->begin();
			size_type i = 0;
			while (it != this->end())
			{
				if (it == position)
				{
					for (InputIterator fill = first; fill != last; fill++)
					{
						_alloc.construct(tmp + i, *fill);
						i++;
					}
				}
				_alloc.construct(tmp + i, *it);
				i++;
				it++;
			}
			if (position == this->end())
			{
				for (InputIterator fill = first; fill != last; fill++)
				{
					_alloc.construct(tmp + i, *fill);
					i++;
				}
			}
			for (size_type i = 0; i < _size; i++)
				_alloc.destroy(_vector + i);
			_alloc.deallocate(_vector, _capacity);
			_capacity = alloc_cap(_size + n);
			_size = i;
			_vector = tmp;
		}

			// Erase

			iterator erase( iterator pos ) {
				iterator it = pos;
				while (it + 1 != end()) {
					*it = *(it + 1);
					it++;
				}
				_alloc.destroy(_vector + _size - 1);
				_size--;
				return (iterator(pos));
			}

			iterator erase( iterator first, iterator last ) {
				while (first != last) {
					erase(first);
					last--;
				}
				return (iterator(first));
			}

			// Push_back

			void push_back( const T& value ) {
				if (_size + 1 > _capacity) {
					if (_capacity == 0)
						reserve(1);
					else
						reserve(_capacity * 2);
				}
				_alloc.construct(_vector + _size++, value);
			}

			// Pop_back

			void pop_back() {
				if (_size > 0) {
					_alloc.destroy(_vector + _size - 1);
					_size -= 1;
				}
			}

			// Resize

			void resize( size_type count, T value = T() ) {
				if (count > _capacity) {
					if (count > _capacity * 2)
						reserve(count);
					else if (_capacity > 0)
						reserve(_capacity * 2);
					else
						reserve(1);
				}
				while (count > _size)
					push_back(value);
				while (count < _size)
					pop_back();
			}

			// Swap

			void swap( vector& other ) {
				ft::swap(_alloc, other._alloc);
				ft::swap(_size, other._size);
				ft::swap(_vector, other._vector);
				ft::swap(_capacity, other._capacity);
			}
	};


	template< class T, class Alloc >
	bool operator==( const ft::vector<T,Alloc>& lhs, const ft::vector<T,Alloc>& rhs ) {
		if (lhs.size() != rhs.size())
			return false;
		const T* lhs_data = lhs.data();
		const T* rhs_data = rhs.data();

		for (std::size_t i = 0; i < lhs.size(); i++)
		{
			if (lhs_data[i] != rhs_data[i])
				return false;
		}
		return true;
	}

	template< class T, class Alloc >
	bool operator!=( const ft::vector<T,Alloc>& lhs, const ft::vector<T,Alloc>& rhs ) {
		if (lhs == rhs)
			return false;
		return true;
	}

	template< class T, class Alloc >
	bool operator>( const ft::vector<T,Alloc>& lhs, const ft::vector<T,Alloc>& rhs ) {
		return ft::lexicographical_compare(rhs.begin(), rhs.end(), lhs.begin(), lhs.end());
	}

	template< class T, class Alloc >
	bool operator<( const ft::vector<T,Alloc>& lhs, const ft::vector<T,Alloc>& rhs ) {
		if (lhs > rhs || lhs == rhs)
			return false;
		return true;
	}

	template< class T, class Alloc >
	bool operator<=( const ft::vector<T,Alloc>& lhs, const ft::vector<T,Alloc>& rhs ) {
		if (lhs < rhs || lhs == rhs)
			return true;
		return false;
	}

	template< class T, class Alloc >
	bool operator>=( const ft::vector<T,Alloc>& lhs, const ft::vector<T,Alloc>& rhs ) {
		if (lhs > rhs || lhs == rhs)
			return true;
		return false;
	}

	template< class T, class Alloc >
	void swap( ft::vector<T,Alloc>& lhs, ft::vector<T,Alloc>& rhs ) {
		lhs.swap(rhs);
	}
};

#endif
