//
//	Made by lejulien
//

#ifndef ENABLE_IF
# define ENABLE_IF

namespace ft {

	template<bool B, class T = void>
	struct enable_if {};
 
	template<class T>
	struct enable_if<true, T> { typedef T type; };

	template<class T, class V>
	struct is_equal {
		static const bool value = false;
	};

	template<class T>
	struct is_equal<T, T> {
		static const bool value = true;
	};


	template <class InputIt1, class InputIt2>
	bool equal(InputIt1 lit, InputIt1 lend, InputIt2 rit, InputIt2 rend) {
		while (lit != lend) {
			if (rit == rend || *rit != *lit)
				return false;
			++lit, ++rit;
		}
		return rit == rend;
	}

	template <class T>
	bool equal(T & a, T & b) {
		return a == b;
	}


};

#endif

//	Made by lejulien@42
