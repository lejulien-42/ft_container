/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lejulien <lejulien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/14 02:32:25 by lejulien          #+#    #+#             */
/*   Updated: 2021/11/19 16:57:11 by lejulien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_HPP
# define STACK_HPP
#include "vector.hpp"
#include <cstddef>

namespace ft {

	template< class T, class Container = ft::vector<T> >
	class stack {
		protected:
			Container c;
		public:
			typedef Container container_type;
			typedef typename Container::value_type value_type;
			typedef typename Container::size_type size_type;
			typedef typename Container::reference reference;
			typedef typename Container::const_reference const_reference;
				

			explicit stack( const Container& cont = Container() ) : c(cont) {
			};

			stack& operator=( const stack& other ) {
				this->c = other.c;
				return *this;
			};

			size_type size() const {
				return this->c.size();
			};

			bool empty() const {
				if (this->c.empty())
					return true;
				return false;
			};

			void push( const value_type& value ) {
				this->c.push_back(value);
			};

			reference top() {
				return (this->c.back());
			};

			const_reference top() const {
				return (this->c.back());
			};

			void pop() {
				this->c.pop_back();
			};

			friend bool operator==( const ft::stack<T,Container>& lhs, const ft::stack<T,Container>& rhs ) {
				return (lhs.c == rhs.c);
			}

			friend bool operator!=(const stack<T,Container>& lhs, const stack<T,Container>& rhs) {
				return (lhs.c != rhs.c);
			}

			friend bool operator<( const ft::stack<T,Container>& lhs, const ft::stack<T,Container>& rhs ) {
				return (lhs.c < rhs.c);
			}

			friend bool operator>(const stack<T,Container>& lhs, const stack<T,Container>& rhs) {
				return (lhs.c > rhs.c);
			}
			friend bool operator<=( const ft::stack<T,Container>& lhs, const ft::stack<T,Container>& rhs ) {
				return (lhs.c <= rhs.c);
			}

			friend bool operator>=(const stack<T,Container>& lhs, const stack<T,Container>& rhs) {
				return (lhs.c >= rhs.c);
			}
	};

};
#endif
