//
//	Made by lejulien
//


#ifndef REVERSE_ITERATOR_HPP
# define REVERSE_ITERATOR_HPP
#include <cstddef>
namespace ft
{
template <class Iter>
	class reverse_iterator {
	private:
		Iter		_Iter;
	public:
		typedef std::size_t	 size_t;
		reverse_iterator(void) {
			_Iter = Iter();
		}
		reverse_iterator(typename Iter::value_type * ptr)	{
			_Iter = Iter(ptr);
		}
		reverse_iterator(const Iter & other) {
			_Iter = other; --_Iter;
		}
		~reverse_iterator(void) {}
		template <class U>
		friend class reverse_iterator;
		template <class U>
		reverse_iterator(const reverse_iterator<U> & other)	{
			_Iter = other.getIt();
		}

		Iter	base(void) {
			return ++Iter(_Iter);
		}
		Iter	getIt(void) const {
			return _Iter;
		}

		typename Iter::value_type &operator[](size_t n) const {
			return *(_Iter - n);
		}
		typename Iter::value_type &operator*(void) const {
			return *_Iter;
		}
		typename Iter::value_type *operator->(void) const {
			return &(*_Iter);
		}

		reverse_iterator &operator=(const reverse_iterator &other) {
			_Iter = other.getIt();
			return *this;
		}
		reverse_iterator &operator+=(int n) {
			_Iter -= n;
			return *this;
		}
		reverse_iterator &operator-=(int n) {
			_Iter += n;
			return *this;
		}
		reverse_iterator &operator++(void) {
			--_Iter;
			return *this;
		}
		reverse_iterator &operator--(void) {
			++_Iter;
			return *this;
		}
		reverse_iterator operator++(int) {
			reverse_iterator<Iter> other(*this);
			--_Iter;
			return other;
		}
		reverse_iterator operator--(int) {
			reverse_iterator<Iter> other(*this);
			++_Iter;
			return other;
		}
		reverse_iterator operator+(int n) const {
			return _Iter - n + 1;
		}
		reverse_iterator operator-(int n) const {
			return _Iter + n + 1;
		}
		std::ptrdiff_t operator-(const reverse_iterator & other) const {
			return other.getIt() - _Iter;
		}
		friend reverse_iterator operator+(int n, const reverse_iterator & other) {
			return other.getIt() - n + 1;
		}
		
		template <class U>
		bool operator==(const reverse_iterator<U> &other) const {
			return _Iter == other.getIt();
		}
		template <class U>
		bool operator!=(const reverse_iterator<U> &other) const {
			return _Iter != other.getIt();
		}
		template <class U>
		bool operator<(const reverse_iterator<U> &other) const {
			return _Iter > other.getIt();
		}
		template <class U>
		bool operator>(const reverse_iterator<U> &other) const {
			return _Iter < other.getIt();
		}
		template <class U>
		bool operator<=(const reverse_iterator<U> &other) const {
			return _Iter >= other.getIt();
		}
		template <class U>
		bool operator>=(const reverse_iterator<U> &other) const {
			return _Iter <= other.getIt();
		}
	};
};

#endif

// Made by lejulien@42
