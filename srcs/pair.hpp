/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pair.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lejulien <lejulien@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/13 23:37:40 by lejulien          #+#    #+#             */
/*   Updated: 2021/08/26 06:06:04 by lejulien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PAIR_HPP
# define PAIR_HPP


namespace ft {
	template <class T1, class T2 >
	struct pair
	{
		private:
		public:
			typedef T1 first_type;
			typedef T2 second_type;
			T1 first;
			T2 second;
			pair() : first(), second() { };
			pair(const T1& x, const T2& y) : first(x), second(y) {
			};
			template< class U1, class U2 >
			pair( const pair<U1, U2>& p ) : first(p.first), second(p.second) {
			};
			pair& operator=( const pair& other ) {
				this->first = other.first;
				this->second = other.second;
				return *this;
			}
	};

	template< class T1, class T2 >
	bool operator==( const ft::pair<T1,T2>& lhs, const ft::pair<T1,T2>& rhs ) {
		if (lhs.first == rhs.first && lhs.second == rhs.second)
			return true;
		return false;
	};
	template< class T1, class T2 >
	bool operator!=( const ft::pair<T1,T2>& lhs, const ft::pair<T1,T2>& rhs ) {
		if (!(lhs == rhs))
			return true;
		return false;	
	};
	template< class T1, class T2 >
	bool operator<( const ft::pair<T1,T2>& lhs, const ft::pair<T1,T2>& rhs ) {
		if (lhs.first < rhs.first)
			return true;
		else if (rhs.first < lhs.first)
			return false;
		else if (lhs.second < rhs.second)
			return true;
		return false;
	};
	template< class T1, class T2 >
	bool operator<=( const ft::pair<T1,T2>& lhs, const ft::pair<T1,T2>& rhs ) {
		if (!(rhs < lhs))
			return true;
		return false;
	};
	template< class T1, class T2 >
	bool operator>( const ft::pair<T1,T2>& lhs, const ft::pair<T1,T2>& rhs ) {
		if (rhs < lhs)
			return true;
		return false;
	};
	template< class T1, class T2 >
	bool operator>=( const ft::pair<T1,T2>& lhs, const ft::pair<T1,T2>& rhs ) {
		if (!(lhs < rhs))
			return true;
		return false;
	};
};

#endif
